# timing programs is inconsistent
# counting operations can characterize efficiency, doesn't tell which operations to count
# 
# want to express complexity as orders of growth
# want to evaluate efficienty when input is big
# want to express growth of run time when input grows
# want to put upper bound on growth
# 
# big oh notation
# puts upper bound on behavior 
# worst case asymptotic complexity: can ignore additive and multiplicative constants
# oh notation can be put as O to the dominant factor O(n) O(log n)
#  